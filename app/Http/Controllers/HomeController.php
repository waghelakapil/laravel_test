<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      
           $comments = Comment::all();
           
          // dd(count($comments));
         /*  if (count($comments) == 0 ) {
               return "No Comments found in the table";
           } else {
            return view('home',compact('comments'));
           }*/
           return view('home',compact('comments'));
           
        
       
    }
}
