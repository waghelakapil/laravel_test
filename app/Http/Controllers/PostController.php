<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

use App\Post;

use Validator;

class PostController extends Controller
{
    public function insertpost(Request $request)
    {

       // dd('sitaram');
        try {
            
                 if($request->ajax())
                 {
                     
                     $rules = array(
                         'title' => 'required',
                         'description' => 'required',
                        'comment.*'=>'required'
                     );

                     $error = Validator::make($request->all(),$rules);

                     if ($error->fails()) {
                         return response()->json([
                            'error'=> $error->$errors()->all()
                         ]);
                     }
                     
                     $title = $request->title;
                     $description = $request->description;

                  $post = new Post;
                  $post->title = $title;
                  $post->description = $description;
                  $post->save();

                  $postid = Post::where([
                      ['title','=',$title],
                      ['description','=',$description]

                      ])->value('id');

                     $count = 0;
                     $comments = $request->comment;

                    foreach ($comments as $comment) {
                         
                        
                        $data = array(
                            'post_id'=>$postid,
                            'comment'=>$comment,
                            'created_at'=> date('Y-m-d'),
                            'updated_at'=>  date('Y-m-d')
                        );
                        $count++;
                        $insert_data[] = $data; 
                     }

                     Comment::insert($insert_data);
                    return response()->json([

                        'success' => 'Data Added successfully'
                    ]);
                 }

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
