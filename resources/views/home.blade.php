<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h1 class="text-center">Welcome</h1>
    <form id="post_form">
        {{ csrf_field() }}
        <span id="result"></span>
        <div class="form-group">
          <label for="exampleFormControlInput1">Title :</label>
          <input type="text" class="form-control" id="title" name="title">
        </div>
       
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Description :</label>
          <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="">
            <table class="table" id="comment_table">
                <thead>
                <tr>
                  <td>  Comment</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="1" align="right">&nbsp;</td>
                        <td>
                       
                            <input type="submit" name="save" id="save" class="btn btn-primary" value="Save">
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
      </form>
    </div>
    <div class="container" id="comment_result" style="list-style-type: none">
        <h3>Comments:</h3>
        @foreach ($comments as $comment)
    <tr><td><input type="text" class="form-control" value="{{$comment->comment}}" ></td></tr><br>
        @endforeach
</div>

    <script>
         var i = 1;
$(document).ready(function() {
      
      var count = 1;

      dynamic_field(count);

      function dynamic_field(number) {

          var html = '<tr>';

          

          html+='<td><input type="text" name="comment[]" class="form-control"/></td>';
          if (number > 1) {
              
              html+='<td><button type="button" name="remove" id="remove" class="btn btn-danger">Remove</button></td></tr>';

              $('tbody').append(html);

          } else {


            html+='<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';

            $('tbody').html(html);
              
          }
          
      }


      $(document).on('click', '#add', function(){
                count++;
                console.log(count);
            dynamic_field(count);
        });


        $(document).on('click', '#remove', function(){
            count--;
          $(this).closest("tr").remove();
        });









        
      $('#post_form').on('submit',function (event) {

       // event.preventDefault();

        $.ajax({
             method:'post',
            url:'{{ route("insertpost")}}',
            data:$('#post_form').serialize(),
            dataType:'json',
            beforeSend:function () {
            $('#save').attr('disabled','disabled');
        },
         success:function(data) {
             console.log(data);

        if (data.error) {

            var error_html = '';

            for (var count = 0; count < data.error.length; count++) {
                
                error_html+='<p>'+data.error[count]+'</p>';

            }
            $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
            
        } else {

            dynamic_field(1);


           // console.log(data);
           
            $('#result').html('<div class="alert alert-success">'+data.success+'</div>');

           
            
            }
        $('#save').attr('disabled',false);
        
         }  
    })

});

});
    </script>

   <script>


         </script>
</body>
</html>